const _ = document;
const CART = [];
const ITEMS = [
    {
        id:1,
        name:'Iphone',
        price:1000,
        discount:15,
        url:'images/iphone.jpg',
    },
    {
        id:2,
        name:'One Million',
        price:100,
        discount:30,
        url:'images/oneMillion.jpg',
    }
];

const ITEMSLIST = _.getElementById('itemsList');
for(let i=0;i<ITEMS.length;i++){
    let newPrice = ITEMS[i].price - ((ITEMS[i].price * ITEMS[i].discount)/100);

    ITEMSLIST.innerHTML +=`<div class="col-sm-4"><div class="card" style="width: 20rem;">`+
        `<img src="${ITEMS[i].url}" class="card-img-top" alt="${ITEMS[i].name}">`+
        `<div class="card-body"><h5 class="card-title">${ITEMS[i].name}</h5>`+
        `<p class="card-text"><span>Was: ${ITEMS[i].price}</span><strong> ${newPrice}</strong></p>`+
        `<button class="btn btn-primary add-to-cart" data-item-id="${ITEMS[i].id}">Add To Cart</button>`
}
ITEMSLIST.addEventListener('click',addToCart);

function addToCart(event) {
    const ELEM = event.target;
    console.log(ELEM)
    if(ELEM.tagName === 'BUTTON' && ELEM.classList.contains('add-to-cart')){
        for (let i=0;i<ITEMS.length;i++){
            if(ITEMS[i].id == ELEM.dataset.itemId){
                CART.push(ITEMS[i]);
                console.log(CART)
            };
        }

    }
    myCart()
}

function myCart() {
    let cart = _.getElementById('cart');
    if(CART.length == 0){
        cart.style.display='none'
    }else{
        for(let i=0;i<CART.length;i++){
            let count = 1;
            let newPrice = (CART[i].price - ((CART[i].price * CART[i].discount)/100)*count);
            cart.innerHTML += `<div><h1>${CART.length}</h1><h3>${CART[i].name}</h3> `+
                `<p>${newPrice}</p>`+`<input type="number" value="${count}"></div>`
        }
    }
}

const   NewNAME = _.getElementById('name'),
        NewPRICE = _.getElementById('price'),
        NewDISCOUNT = _.getElementById('discount'),
        NewURL = _.getElementById('url'),
        ADDNEWITEM = _.getElementById('add-item');

ADDNEWITEM.addEventListener('click',addNewItem);


function addNewItem(event) {
    const ELEM = event.target;
    console.log(ELEM)
    if(ELEM.tagName === 'BUTTON' && ELEM.classList.contains('add-new-item')){
        let newItem = [
            {
                id:Date.now(),
                name:NewNAME.value,
                price:NewPRICE.value,
                discount:NewDISCOUNT.value,
                url:'images/'+ NewURL.value,
            }
        ]
        console.log('new' + newItem)
        ITEMSLIST.push(newItem);
    }

    console.log(ITEMSLIST);
}





